package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;

public interface JSONWalletBonusDeserializer extends DirectAccessDatabaseDeserializer {

    DigitalBadge deserialize(WalletFrameMedia media, DigitalBadgeMetadata metas) throws IOException;
    DigitalBadge deserialize(WalletFrameMedia media, long pos) throws IOException;

}
