package fr.cnam.foad.nfa035.badges.gui.view;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component("displayedBadgeHolder")
@Order(value = 1)
public class DisplayedBadgeHolder {

    private DigitalBadge displayedBadge;

    public DigitalBadge getDisplayedBadge() {
        return displayedBadge;
    }

    public void setDisplayedBadge(DigitalBadge displayedBadge) {
        this.displayedBadge = displayedBadge;
    }
}